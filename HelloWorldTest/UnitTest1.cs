﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace HelloWorldTest
{
    [TestClass]
    public class UnitTest1
    {
        [TestMethod]
        public void TestHelloWorld()
        {
            string hello = HelloJenkins.Program.HelloWorld();
            Assert.AreEqual("Hello World!", hello);
        }
    }
}
